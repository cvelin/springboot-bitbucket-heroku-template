# springboot-bitbucket-heroku-template

This template provides the basis to get a spring-boot application up and running on Heroku with minimal effort.

## Requirements

- a Heroku account with a generated API key

## Change this

In `pom.xml`:

- artifactId
- name
- description

In sources:

- Refactor -> rename `ApplicationName.java`

In BitBucket (repo->settings->repository variables):

- Add `HEROKU_API_KEY` and set value to your Heroku API key
- Add `HEROKU_APP_NAME` and set value to your app name

In BitBucket(repo->settings->pipelines->settings):

- Enable pipelines
